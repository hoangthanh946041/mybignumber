
    class MyBigNumber
    {
        public string sum (String stn1, String stn2)
        {
            
                string result = "";
                int len1 = stn1.Length; 
                int len2 = stn2.Length; 
                int maxLen = len1 > len2 ? len1 : len2; 
                int soNho = 0; 
                int temp = 0; 


                for (int i = 1; i <= maxLen; i++)
                {
                    int num1 = i > len1 ? 0 : stn1[len1 - i] - '0'; 
                    int num2 = i > len2 ? 0 : stn2[len2 - i] - '0'; 

                    temp = num1 + num2 + soNho;

                    if (temp >= 10)
                    {
                        soNho = 1;
                        temp -= 10;
                    }
                    else
                    {
                        soNho = 0;
                    }

                    result = temp.ToString() + result;
                }

                if (soNho > 0)
                {
                    result = "1" + result;
                }
        }
    }

